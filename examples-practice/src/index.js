import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes, useNavigate, useLocation } from 'react-router-dom'

import './index.css';
import Basic from './V7/basic.tsx'

const routes = [
  {
    path: '/',
    component: Basic,
  }
]

const Header = () => {
  const navigate = useNavigate()
  const location = useLocation()
  const onChange = (event) => navigate(event.target.value)

  return (
    <header>
      <select defaultValue={location.pathname} onChange={onChange}>
        {routes.map((route) => (
          <option value={route.path} key={route.path}>
            {route.path === '/' ? 'basic' : route.path.substring(1, route.path.length)}
          </option>
        ))}
      </select>
    </header>
  )
}

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <BrowserRouter>
    <Header />
    <Routes>
      {routes.map((route) => (
        <Route path={route.path} key={route.path} element={<route.component />} />
      ))}
    </Routes>
  </BrowserRouter>
)